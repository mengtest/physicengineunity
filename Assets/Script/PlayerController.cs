﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    private SphereShapeCollider sphereShape;
    public Vector3 baseSpeed = new Vector3(-0.5f, -0.5f, 0);

    private void Start()
    {
        PhysicSystem.Instance.AddController(this);
        sphereShape = GetComponent<SphereShapeCollider>();
    }

    public void UpdatePhysic()
    {
        var moveValue = baseSpeed * Time.fixedDeltaTime;
        sphereShape.Move(moveValue);
    }
}
