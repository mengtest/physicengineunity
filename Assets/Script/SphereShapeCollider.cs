﻿using UnityEngine;
using System.Collections;

public class SphereShapeCollider : PhysicCollider {
    public float Radius = 0.5f;
    private Vector3 motionValue = Vector3.zero;
    public float SlotLimit = 45;

    protected override void Awake()
    {
        base.Awake();
        colliderType = ColliderType.Sphere;
    }
    private void Start()
    {
        PhysicSystem.Instance.AddCollider(this);
    }
    private void OnDrawGizmos()
    {
        if (IsCollision)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(transform.position, Radius + 0.1f);
        }
    }



    public override void ApplyContact(PhysicContact contact)
    {
        /*
        var normal = contact.Normal;
        if(contact.B == this)
        {
            normal = -normal;
        }

        solverWorkArea.deltaVelocity += -Vector3.Dot(velocity, normal) * normal;
        */
    }

    //相当于加速度 速度目标是 (curPos-lastPosition) / Time.fixedDeltaTime 正常速度
    //沿着斜坡走的速度不会降低 如果碰撞的话 
    public override void UpdatePosition()
    {
        /*
        var curPos = transform.position;
        var newPos = (2) * curPos - (1) * lastPosition + accelerate * Time.fixedDeltaTime * Time.fixedDeltaTime;
        lastLastPosition = lastPosition;
        lastPosition = curPos;
        transform.position = newPos+motionValue;
        */
        lastPosition = transform.position;
        transform.position += motionValue;
    }

    //Time.deltaFixedTime 一帧率移动量
    public void Move(Vector3 moveValue)
    {
        motionValue = moveValue;
    }
    public override void Clear()
    {
        motionValue = Vector3.zero;
    }
}
