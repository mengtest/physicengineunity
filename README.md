# PhysicEngineUnity

#### 介绍
Unity 自定义物理引擎，模拟旺达与巨像

通过对SkinnedMeshRender 进行bake 获得动画当前帧的Mesh结构

计算Sphere 和Triangle Mesh 之间的物理碰撞 

参考物理代码

  http://realtimecollisiondetection.net/blog/?p=103
    
  http://www.realtimerendering.com/intersections.html
    
  https://github.com/gszauer/GamePhysicsCookbook